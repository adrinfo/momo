package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import info.adri.momo.R;
import model.Variables;
import model.ProjectTask;

/**
 * <p class="description">Adapter of done project tasks</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class ProjectTaskAdapterDone extends RecyclerView.Adapter<ProjectTaskAdapterDone.ProjectTaskDoneViewHolder> {

    private Context context;
    private LongClickListener longClickListener;

    public ProjectTaskAdapterDone(Context context) {
        this.context = context;
    }

    @Override
    public ProjectTaskDoneViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_project_tasks, parent, false);
        return new ProjectTaskDoneViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return Variables.projectTasksDone.size();
    }

    @Override
    public void onBindViewHolder(ProjectTaskDoneViewHolder projectTaskViewHolder, int position) {

        ProjectTask projectTask = Variables.projectTasksDone.get(position);

        projectTaskViewHolder.id.setText(String.valueOf(projectTask.getId()));
        projectTaskViewHolder.position = position;
        projectTaskViewHolder.task.setText(projectTask.getTask());


    }


    class ProjectTaskDoneViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        private TextView task, id;
        private LinearLayout card;
        private int position;

        public ProjectTaskDoneViewHolder(View itemView) {
            super(itemView);

            card = (LinearLayout) itemView.findViewById(R.id.llPTCard);
            task = (TextView) itemView.findViewById(R.id.tvProjectTask);
            id = (TextView) itemView.findViewById(R.id.tvProjectId);

            card.setOnLongClickListener(this);

        }

        @Override
        public boolean onLongClick(View v) {
            longClickListener.itemLongClicked(v, position, Integer.valueOf(id.getText().toString()));
            return false;
        }
    }

    public void setLongClickListener(LongClickListener longClickListener)  {
        this.longClickListener = longClickListener;
    }

    public interface LongClickListener {
        void itemLongClicked(View view, int position, int id);
    }
}
