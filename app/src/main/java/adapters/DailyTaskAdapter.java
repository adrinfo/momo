package adapters;

import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import info.adri.momo.R;
import model.Variables;
import model.DailyTask;

/**
 * <p class="description">Adapter of daily tasks</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class DailyTaskAdapter extends RecyclerView.Adapter<DailyTaskAdapter.DailyTaskViewHolder> {


    private Context context;
    private FragmentActivity activity;
    private ClickListener clickListener;
    private LongClickListener longClickListener;

    public DailyTaskAdapter(Context context, FragmentActivity activity) {

        this.context = context;
        this.activity = activity;
    }

    @Override
    public DailyTaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_daily_tasks, parent, false);
        return new DailyTaskViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DailyTaskViewHolder dailyTaskViewHolder, int position) {

        DailyTask dailyTask = Variables.dailyTasks.get(position);

        if(dailyTask.getCompleted() == 1) {
            dailyTaskViewHolder.card.setSelected(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dailyTaskViewHolder.card.setElevation(2);
                dailyTaskViewHolder.card.setBackgroundResource(R.drawable.ripple_effect_selected);
            }
            dailyTaskViewHolder.task.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            dailyTaskViewHolder.icon.setColorFilter(context.getResources().getColor(R.color.colorPrimary));
        } else {
            dailyTaskViewHolder.card.setSelected(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dailyTaskViewHolder.card.setBackgroundResource(R.drawable.ripple_effect);
                dailyTaskViewHolder.card.setElevation(8);
            }
            dailyTaskViewHolder.task.setTextColor(context.getResources().getColor(R.color.colorPrimaryText));
            dailyTaskViewHolder.icon.setColorFilter(context.getResources().getColor(R.color.colorPrimaryText));
        }

        dailyTaskViewHolder.id.setText(String.valueOf(dailyTask.getId()));
        dailyTaskViewHolder.position = position;
        dailyTaskViewHolder.icon.setImageResource(context.getResources().getIdentifier(dailyTask.getIcon(), "drawable", activity.getPackageName()));
        dailyTaskViewHolder.task.setText(dailyTask.getTask());
        dailyTaskViewHolder.category.setText(dailyTask.getCategory().toUpperCase());
        if(dailyTask.getGoal() == 1) {
            dailyTaskViewHolder.goal.setText("");
        } else {
            dailyTaskViewHolder.goal.setText(String.valueOf(dailyTask.getGoal()));
        }
    }

    public void setClickListener(ClickListener clickListener)  {
        this.clickListener = clickListener;
    }

    public void setLongClickListener(LongClickListener longClickListener)  {
        this.longClickListener = longClickListener;
    }

    @Override
    public int getItemCount() {
        return Variables.dailyTasks.size();
    }

    class DailyTaskViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private ImageView icon;
        private TextView task, goal, id, category;
        private LinearLayout card;
        private int position;

        public DailyTaskViewHolder(View itemView) {
            super(itemView);

            category = (TextView) itemView.findViewById(R.id.tvCategoryDTV);
            card = (LinearLayout) itemView.findViewById(R.id.llDTCard);
            icon = (ImageView) itemView.findViewById(R.id.ivIconDTV);
            task = (TextView) itemView.findViewById(R.id.tvTaskDTV);
            goal = (TextView) itemView.findViewById(R.id.tvGoalDTV);
            id = (TextView) itemView.findViewById(R.id.tvId);

            card.setOnClickListener(this);
            card.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int goalTemp = 0;
            if(!goal.getText().toString().isEmpty()) {
                goalTemp = Integer.valueOf(goal.getText().toString());
            }
            clickListener.itemClicked(v, position, Integer.valueOf(id.getText().toString()), goalTemp);

        }

        @Override
        public boolean onLongClick(View v) {
            longClickListener.itemLongClicked(v, position, Integer.valueOf(id.getText().toString()));
            return false;
        }
    }

    public interface ClickListener {
        public void itemClicked(View view, int position, int id, int goal);
    }

    public interface LongClickListener {
        public void itemLongClicked(View view, int position, int id);
    }




}
