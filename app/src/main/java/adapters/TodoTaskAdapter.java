package adapters;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import info.adri.momo.R;
import model.Variables;
import model.TodoTask;

/**
 * <p class="description">Adapter of todoTask</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class TodoTaskAdapter extends RecyclerView.Adapter<TodoTaskAdapter.TodoTaskViewHolder> {

    private Context context;
    private ClickListener clickListener;
    private LongClickListener longClickListener;

    public TodoTaskAdapter(Context context) {
        this.context = context;
    }

    @Override
    public TodoTaskAdapter.TodoTaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_todo_tasks, parent, false);
        return new TodoTaskViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return Variables.todoTasks.size();
    }

    @Override
    public void onBindViewHolder(TodoTaskAdapter.TodoTaskViewHolder todoTaskViewHolder, int position) {

        TodoTask todoTask = Variables.todoTasks.get(position);

        if(todoTask.getCompleted() == 1) {
            todoTaskViewHolder.card.setSelected(true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                todoTaskViewHolder.card.setElevation(2);
                todoTaskViewHolder.card.setBackgroundResource(R.drawable.ripple_effect_selected);
            }
            todoTaskViewHolder.task.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                todoTaskViewHolder.card.setElevation(8);
                todoTaskViewHolder.card.setBackgroundResource(R.drawable.ripple_effect);
            }
            todoTaskViewHolder.task.setTextColor(context.getResources().getColor(R.color.colorPrimaryText));
        }

        todoTaskViewHolder.id.setText(String.valueOf(todoTask.getId()));
        if(todoTask.getCompleted() == 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                todoTaskViewHolder.card.setBackgroundResource(R.drawable.ripple_effect);
            }
        } else if(todoTask.getCompleted() == 1) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                todoTaskViewHolder.card.setBackgroundResource(R.drawable.ripple_effect_selected);
            }
            todoTaskViewHolder.task.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }
        todoTaskViewHolder.task.setText(todoTask.getTask());
        todoTaskViewHolder.category.setText(todoTask.getCategory());
        todoTaskViewHolder.position = position;

    }


    class TodoTaskViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView task, category, id;
        private LinearLayout card;
        private int position;

        public TodoTaskViewHolder(View itemView) {
            super(itemView);

            id = (TextView) itemView.findViewById(R.id.tvTodoId);
            task = (TextView) itemView.findViewById(R.id.tvTodoTask);
            category = (TextView) itemView.findViewById(R.id.tvTodoCategory);
            card = (LinearLayout) itemView.findViewById(R.id.llTTCard);

            card.setOnClickListener(this);
            card.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListener.itemClicked(v, position, Integer.valueOf(id.getText().toString()));
        }

        @Override
        public boolean onLongClick(View v) {
            longClickListener.itemLongClicked(v, position, Integer.valueOf(id.getText().toString()));
            return false;
        }
    }

    public void setClickListener(ClickListener clickListener)  {
        this.clickListener = clickListener;
    }

    public void setLongClickListener(LongClickListener longClickListener)  {
        this.longClickListener = longClickListener;
    }

    public interface ClickListener {
        public void itemClicked(View view, int position, int id);
    }

    public interface LongClickListener {
        public void itemLongClicked(View view, int position, int id);
    }

}


