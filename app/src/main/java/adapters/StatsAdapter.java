package adapters;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import info.adri.momo.R;
import model.Stats;
import model.Variables;

/**
 * <p class="description">Adapter of statistics</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class StatsAdapter extends RecyclerView.Adapter<StatsAdapter.StatsViewHolder> {

    private Context context;

    public StatsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public StatsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_stats, parent, false);
        return new StatsViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return Variables.finalStats.size();
    }

    @Override
    public void onBindViewHolder(StatsViewHolder holder, int position) {

        Stats stats = Variables.finalStats.get(position);

        holder.task.setText(stats.getTask());
        if(android.os.Build.VERSION.SDK_INT >= 11){
            ObjectAnimator animation = ObjectAnimator.ofInt(holder.pbStats, "progress", (stats.getGoalCompleted() * 1000) / stats.getGoal());
            animation.setDuration(500);
            animation.setInterpolator(new DecelerateInterpolator());
            animation.start();
        }
        else
            holder.pbStats.setProgress((stats.getGoalCompleted() * 1000) / stats.getGoal());




    }

    public class StatsViewHolder extends RecyclerView.ViewHolder {

        private TextView task;
        private ProgressBar pbStats;

        public StatsViewHolder(View itemView) {
            super(itemView);

            task = (TextView) itemView.findViewById(R.id.tvTaskStats);
            pbStats = (ProgressBar) itemView.findViewById(R.id.pbStats);

        }

    }

}
