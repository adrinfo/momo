package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import info.adri.momo.R;
import model.Variables;
import model.ProjectTask;

/**
 * <p class="description">Adapter of to do project tasks</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class ProjectTaskAdapterTodo extends RecyclerView.Adapter<ProjectTaskAdapterTodo.ProjectTaskTodoViewHolder> {

    private Context context;
    private ClickListener clickListener;
    private LongClickListener longClickListener;

    public ProjectTaskAdapterTodo(Context context) {
        this.context = context;
    }

    @Override
    public ProjectTaskTodoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_project_tasks, parent, false);
        return new ProjectTaskTodoViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return Variables.projectTasksTodo.size();
    }

    @Override
    public void onBindViewHolder(ProjectTaskTodoViewHolder projectTaskViewHolder, int position) {

        ProjectTask projectTask = Variables.projectTasksTodo.get(position);

        projectTaskViewHolder.id.setText(String.valueOf(projectTask.getId()));
        projectTaskViewHolder.position = position;
        projectTaskViewHolder.task.setText(projectTask.getTask());


    }


    class ProjectTaskTodoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView task, id;
        private LinearLayout card;
        private int position;

        public ProjectTaskTodoViewHolder(View itemView) {
            super(itemView);

            card = (LinearLayout) itemView.findViewById(R.id.llPTCard);
            task = (TextView) itemView.findViewById(R.id.tvProjectTask);
            id = (TextView) itemView.findViewById(R.id.tvProjectId);

            card.setOnClickListener(this);
            card.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListener.itemClicked(v, position, Integer.valueOf(id.getText().toString()));
        }

        @Override
        public boolean onLongClick(View v) {
            longClickListener.itemLongClicked(v, position, Integer.valueOf(id.getText().toString()));
            return false;
        }
    }

    public void setClickListener(ClickListener clickListener)  {
        this.clickListener = clickListener;
    }

    public void setLongClickListener(LongClickListener longClickListener)  {
        this.longClickListener = longClickListener;
    }

    public interface ClickListener {
        void itemClicked(View view, int position, int id);
    }

    public interface LongClickListener {
        void itemLongClicked(View view, int position, int id);
    }
}
