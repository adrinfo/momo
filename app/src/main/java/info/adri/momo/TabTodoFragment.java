package info.adri.momo;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.HashSet;

import adapters.ProjectTaskAdapterTodo;
import model.Variables;
import model.GenericMessage;
import model.ProjectTask;
import model.ProjectTasks;

/**
 * <p class="description">Handles fragment_tab_todo.xml</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class TabTodoFragment extends Fragment implements ProjectTaskAdapterTodo.ClickListener, ProjectTaskAdapterTodo.LongClickListener {

    static final int INSERTPROJECT = 1, UPDATEPROJECT = 2;
    private RecyclerView rvProjectTasksTodo;
    private FloatingActionButton switchFloatingButtons, createTodo, changeProject;
    private ProgressBar pbProject;
    private boolean invisible;
    private int ilcPosition, ilcID;
    private TextView tvProjectTitle, tvNoContentPT;
    private FrameLayout flProject;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate and get main view
        View rootView;

        // If landscape
        if(getResources().getConfiguration().orientation == 2) {
            rootView = inflater.inflate(R.layout.fragment_tab_todo_land, container, false);
        } else {
            rootView = inflater.inflate(R.layout.fragment_tab_todo, container, false);
        }

        // Get views
        rvProjectTasksTodo = (RecyclerView) rootView.findViewById(R.id.rvProjectTasksTodo);
        switchFloatingButtons = (FloatingActionButton) rootView.findViewById(R.id.switchFloatingButtons);
        createTodo = (FloatingActionButton) rootView.findViewById(R.id.createTodo);
        changeProject = (FloatingActionButton) rootView.findViewById(R.id.changeProject);
        tvProjectTitle = (TextView) rootView.findViewById(R.id.tvProjectTitle);
        tvNoContentPT = (TextView) rootView.findViewById(R.id.tvNoContentPT);
        pbProject = (ProgressBar) rootView.findViewById(R.id.pbProject);
        flProject = (FrameLayout) rootView.findViewById(R.id.flProject);
        invisible = true;

        // Call data initialization
        dataInit();

        // Shows and hides the other buttons
        switchFloatingButtons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(invisible) {
                    flProject.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        v.setElevation(6);
                        v.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryLight)));
                    }
                    invisible = false;
                } else {
                    flProject.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        v.setElevation(12);
                        v.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                    }
                    invisible = true;
                }
            }
        });

        flProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flProject.setVisibility(View.GONE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    switchFloatingButtons.setElevation(12);
                    switchFloatingButtons.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                }
                invisible = true;
            }
        });

        createTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flProject.setVisibility(View.GONE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    switchFloatingButtons.setElevation(12);
                    switchFloatingButtons.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                }
                invisible = true;

                Intent i = new Intent(getActivity(), InsertProjectTaskActivity.class);
                startActivityForResult(i, INSERTPROJECT);
            }
        });

        changeProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flProject.setVisibility(View.GONE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    switchFloatingButtons.setElevation(12);
                    switchFloatingButtons.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                }
                invisible = true;

                createProjectListDialog().show();

            }
        });

        // Instantiate adapter and add listener
        Variables.projectTaskAdapterTodo = new ProjectTaskAdapterTodo(getContext());
        Variables.projectTaskAdapterTodo.setClickListener(this);
        Variables.projectTaskAdapterTodo.setLongClickListener(this);

        // Create layout manager and set to adapter
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rvProjectTasksTodo.setLayoutManager(llm);
        rvProjectTasksTodo.addItemDecoration(new TodoTasksFragment.GapSpace(20));
        rvProjectTasksTodo.setAdapter(Variables.projectTaskAdapterTodo);

        return rootView;
    }

    /**
     * Creates dialog for switching between projects
     */
    public AlertDialog createProjectListDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        int projectsSize = Variables.project.size();
        final CharSequence[] projects = new CharSequence[projectsSize];
        for(int i = 0; i < Variables.project.size(); i++) {
            projects[i] = Variables.project.toArray()[i].toString();
        }

        builder.setTitle(getActivity().getString(R.string.projects))
                .setItems(projects, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Variables.chosenProject = Variables.project.toArray()[which].toString();
                        tvProjectTitle.setText(Variables.chosenProject);
                        tvProjectTitle.setVisibility(View.VISIBLE);
                        final View view = tvProjectTitle;
                        view.postDelayed(new Runnable() { public void run() { view.setVisibility(View.GONE); } }, 3000);
                        reset();
                        dataInit();

                    }
                });

        return builder.create();

    }

    /**
     * Fills the collections with data from the server
     */
    private void dataInit() {

        if(Variables.projectTasksTodo.isEmpty() && Variables.projectTasksDoing.isEmpty() && Variables.projectTasksDone.isEmpty()) {

            pbProject.setVisibility(View.VISIBLE);

            // Load project tasks
            Ion.with(getContext())
                    .load("GET", getString(R.string.uriProjectTasks))
                    .setHeader("Authorization", Variables.apiKey)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {

                        @Override
                        public void onCompleted(Exception e, JsonObject result) {

                            ProjectTasks res = new ProjectTasks();
                            Gson gson = new Gson();
                            res = gson.fromJson(result, res.getClass());

                            if(res != null) {

                                if(res.getTasks().isEmpty()) {

                                    tvNoContentPT.setVisibility(View.VISIBLE);
                                    Variables.chosenProject = "";

                                } else {

                                    tvNoContentPT.setVisibility(View.GONE);

                                    for(ProjectTask task : res.getTasks()) {

                                        Variables.project.add(task.getProject());

                                    }

                                    if(Variables.chosenProject.isEmpty()) {

                                        Variables.chosenProject = Variables.project.toArray()[0].toString();
                                        tvProjectTitle.setText(Variables.chosenProject);
                                        final View view = tvProjectTitle;
                                        view.postDelayed(new Runnable() { public void run() { view.setVisibility(View.GONE); } }, 3000);

                                    }
                                }

                                Log.d("chosenProject", Variables.chosenProject);
                                for(ProjectTask task: res.getTasks()) {
                                    if(task.getProject().equals(Variables.chosenProject)) {
                                        switch(task.getState()) {
                                            case 0:
                                                Variables.projectTasksTodo.add(task);
                                                break;
                                            case 1:
                                                Variables.projectTasksDoing.add(task);
                                                break;
                                            case 2:
                                                Variables.projectTasksDone.add(task);
                                                break;
                                        }
                                        Log.d("PROJECTTASK", task.getTask() + ", project: " + task.getProject() + ", state: " + task.getState());
                                    }
                                }
                                Log.d("HASHSET", Variables.project.toString());

                                pbProject.setVisibility(View.GONE);

                                Variables.projectTaskAdapterTodo.notifyDataSetChanged();
                                Variables.projectTaskAdapterDoing.notifyDataSetChanged();

                            } else {
                                Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
        }

    }

    /**
     * Clears the collections
     */
    public void reset() {

        Variables.project = new HashSet<>();
        Variables.projectTasksTodo = new ArrayList<>();
        Variables.projectTasksDoing = new ArrayList<>();
        Variables.projectTasksDone = new ArrayList<>();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == INSERTPROJECT) {
            if (resultCode == 1) {
                reset();
                dataInit();
                Toast.makeText(getContext(), R.string.inserted_task, Toast.LENGTH_SHORT).show();
            }
        } else if(requestCode == UPDATEPROJECT) {
            if(resultCode == 2) {
                reset();
                dataInit();
                Toast.makeText(getContext(), R.string.updated_task, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void itemClicked(View view, int position, int id) {

        // Pass to the next state
        Ion.with(getContext())
                .load("PUT", getString(R.string.uriProjectTasks) + "/" + id)
                .setHeader("Authorization", Variables.apiKey)
                .setBodyParameter("task", Variables.projectTasksTodo.get(position).getTask())
                .setBodyParameter("project", Variables.projectTasksTodo.get(position).getProject())
                .setBodyParameter("state", "1")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {

                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        GenericMessage res = new GenericMessage();
                        Gson gson = new Gson();
                        res = gson.fromJson(result, res.getClass());

                        if(res != null) {

                            if(res.getError().equals("false")) {
                                reset();
                                dataInit();
                            }

                        }

                    }

                });

    }

    @Override
    public void itemLongClicked(View view, int position, int id) {
        ilcPosition = position;
        ilcID = id;
        registerForContextMenu(view);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if(getUserVisibleHint()) {
            switch(item.getItemId()) {

                case R.id.ctxEdit:
                    ProjectTask tempTask = Variables.projectTasksTodo.get(ilcPosition);
                    Intent i = new Intent(getActivity(), InsertProjectTaskActivity.class);
                    i.putExtra("task", tempTask.getTask());
                    i.putExtra("project", tempTask.getProject());
                    i.putExtra("id", tempTask.getId());
                    i.putExtra("tab", 0);
                    startActivityForResult(i, UPDATEPROJECT);
                    return true;

                case R.id.ctxRemove:

                    Log.d("ID", "" + ilcID);
                    // Remove to do task
                    Ion.with(getContext())
                            .load("DELETE", getString(R.string.uriProjectTasks) + "/" + ilcID)
                            .setHeader("Authorization", Variables.apiKey)
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {

                                @Override
                                public void onCompleted(Exception e, JsonObject result) {

                                    GenericMessage res = new GenericMessage();
                                    Gson gson = new Gson();
                                    res = gson.fromJson(result, res.getClass());

                                    if(res != null) {

                                        Log.d("Error", "" + res.getError());
                                        Log.d("Message", "" + res.getMessage());

                                    } else {
                                        Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                                    }

                                    reset();
                                    dataInit();

                                }

                            });

                default:
                    return super.onContextItemSelected(item);

            }
        }

        return super.onContextItemSelected(item);

    }
}
