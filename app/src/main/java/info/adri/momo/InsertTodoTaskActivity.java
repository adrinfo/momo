package info.adri.momo;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import model.Variables;
import model.GenericMessage;
import model.GenericMessage2;

/**
 * <p class="description">Handles activity_insert_todo_task</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class InsertTodoTaskActivity extends AppCompatActivity {

    final int sizeSeekBarCorrection = 1;
    EditText etTTtask;
    AutoCompleteTextView etTTcategory;
    SeekBar sbTTpriority;
    TextView tvTTpriority, tvTTpriorityState;
    Button btnTTcancelCreate, btnTTcreate;
    String edTask, edCategory;
    int edPriority, edId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_todo_task);

        etTTtask = (EditText) findViewById(R.id.etTTtask);
        etTTcategory = (AutoCompleteTextView) findViewById(R.id.etTTcategory);
        sbTTpriority = (SeekBar) findViewById(R.id.sbTTpriority);
        tvTTpriority = (TextView) findViewById(R.id.tvTTpriority);
        tvTTpriorityState = (TextView) findViewById(R.id.tvTTpriorityState);
        btnTTcancelCreate = (Button) findViewById(R.id.btnTTcancelCreate);
        btnTTcreate = (Button) findViewById(R.id.btnTTcreate);

        // Adapter that recommend categories that already exist
        String[] categories = new String[Variables.todoCategories.size()];
        int i = 0;
        for(String category : Variables.todoCategories) {
            categories[i] = category;
            i++;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, categories);
        etTTcategory.setAdapter(adapter);

        // Bundle (if edit)
        final Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            TextView tvTitleNewTodo = (TextView) findViewById(R.id.tvTitleNewTodo);
            tvTitleNewTodo.setText(R.string.edit_task);
            btnTTcreate.setText(R.string.edit);
            edTask = bundle.getString("task");
            etTTtask.setText(edTask);
            edCategory = bundle.getString("category");
            etTTcategory.setText(edCategory);
            edPriority = bundle.getInt("priority");
            int progress = edPriority + sizeSeekBarCorrection;
            sbTTpriority.setProgress(edPriority);
            tvTTpriorityState.setText(progress + "/10");
            setLegend(progress);
            edId = bundle.getInt("id");

        }

        // Priority seekbar
        sbTTpriority.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            int progress = sbTTpriority.getProgress();

            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue + sizeSeekBarCorrection;
                tvTTpriorityState.setText(progress + "/10");
                setLegend(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                InputMethodManager imm = (InputMethodManager)getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(tvTTpriorityState.getWindowToken(), 0);
            }

        });

        btnTTcreate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (etTTtask.getText().toString().isEmpty()) {
                    Toast.makeText(InsertTodoTaskActivity.this, R.string.task_field_cannot_empty, Toast.LENGTH_SHORT)
                            .show();
                } else {

                    if(bundle == null) {

                        // Create todoTask
                        Ion.with(getBaseContext())
                                .load("POST", getString(R.string.uriTodoTasks))
                                .setHeader("Authorization", Variables.apiKey)
                                .setBodyParameter("task", etTTtask.getText().toString())
                                .setBodyParameter("category", etTTcategory.getText().toString())
                                .setBodyParameter("priority", String.valueOf(sbTTpriority.getProgress()))
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {

                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {

                                        GenericMessage2 res = new GenericMessage2();
                                        Gson gson = new Gson();
                                        res = gson.fromJson(result, res.getClass());

                                        if (res != null) {
                                            if (res.getError().equals("true")) {
                                                Toast.makeText(InsertTodoTaskActivity.this, R.string.error_create_task + res.getMessage(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                setResult(1);
                                                finish();
                                            }
                                        } else {
                                            Toast.makeText(InsertTodoTaskActivity.this, R.string.connection_error_add_task, Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                });

                    } else {

                        // Update todoTask
                        Ion.with(getBaseContext())
                                .load("PUT", getString(R.string.uriTodoTasks) + "/" + edId)
                                .setHeader("Authorization", Variables.apiKey)
                                .setBodyParameter("task", etTTtask.getText().toString())
                                .setBodyParameter("category", etTTcategory.getText().toString())
                                .setBodyParameter("priority", String.valueOf(sbTTpriority.getProgress()))
                                .setBodyParameter("completed", "0")
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {

                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {

                                        GenericMessage res = new GenericMessage();
                                        Gson gson = new Gson();
                                        res = gson.fromJson(result, res.getClass());

                                        if (res != null) {
                                            if (res.getError().equals("true")) {
                                                Toast.makeText(InsertTodoTaskActivity.this, R.string.error_update_task + res.getMessage(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                setResult(2);
                                                finish();
                                            }
                                        } else {
                                            Toast.makeText(InsertTodoTaskActivity.this, R.string.connection_error_update_task, Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                });

                    }

                }

            }

        });

        btnTTcancelCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }

    private void setLegend(int progress) {

        switch (progress) {

            case 1:
            case 2:
            case 3:
                tvTTpriority.setText(R.string.not_urgent);
                break;
            case 4:
            case 5:
            case 6:
                tvTTpriority.setText(R.string.not_so_important);
                break;
            case 7:
            case 8:
                tvTTpriority.setText(R.string.must_do);
                break;
            case 9:
            case 10:
                tvTTpriority.setText(R.string.urgent);
                break;

        }

    }
}
