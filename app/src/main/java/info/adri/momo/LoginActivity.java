package info.adri.momo;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import model.LoginMessage;

/**
 * <p class="description">Checks if there is user data, so it can skip the login screen.</p>
 * <p class="description">If there's no data, it opens the login screen so the user can log in,
 * or opens the register screen so the user can register.</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class LoginActivity extends AppCompatActivity {

    private EditText etEmail, etPass;
    private Button btnEntrar;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        preferences = getSharedPreferences("User", Context.MODE_PRIVATE);

        // If not previous login, show login screen, else open MainActivity
        if(preferences.getString("email", "vacio").equals("vacio")) {

            setContentView(R.layout.activity_login);

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.register);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                    startActivity(i);
                }
            });

            etEmail = (EditText) findViewById(R.id.etEmail);
            etPass = (EditText) findViewById(R.id.etPass);
            btnEntrar = (Button) findViewById(R.id.btnEntrar);

            btnEntrar.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        v.setElevation(8);
                    }

                    // Check if user data exists
                    Ion.with(getBaseContext())
                            .load(getString(R.string.uriLogin))
                            .setBodyParameter("email", etEmail.getText().toString())
                            .setBodyParameter("password", etPass.getText().toString())
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {

                                @Override
                                public void onCompleted(Exception e, JsonObject result) {

                                    LoginMessage res = new LoginMessage();
                                    Gson gson = new Gson();
                                    res = gson.fromJson(result, res.getClass());

                                    if(res != null) {
                                        if(res.getError().equals("true")) {
                                            Toast.makeText(LoginActivity.this, R.string.incorrect_data, Toast.LENGTH_SHORT).show();
                                        } else {
                                            // If user exists saves the data
                                            SharedPreferences prefs = getSharedPreferences("User", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = prefs.edit();
                                            editor.putString("name", res.getName());
                                            editor.putString("email", res.getEmail());
                                            editor.putString("apiKey", res.getApiKey());
                                            editor.commit();
                                            // Opens MainActivity, closes LoginActivity
                                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    } else {
                                        Toast.makeText(LoginActivity.this, R.string.connection_error_identity, Toast.LENGTH_SHORT).show();
                                    }


                                }

                            });

                }

            });

        } else {

            // The user data exists so it skips the login-register part
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
            finish();

        }



    }

}
