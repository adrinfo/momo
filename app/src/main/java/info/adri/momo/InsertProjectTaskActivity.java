package info.adri.momo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import model.Variables;
import model.GenericMessage2;

/**
 * <p class="description">Handles activity_insert_project_task.xml</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class InsertProjectTaskActivity extends AppCompatActivity {

    private EditText etPTtask;
    private AutoCompleteTextView etPTproject;
    private Button btnPTcancelCreate, btnPTcreate;
    private TextView tvTitleNewProject;
    private String edTask, edProject;
    private int edId, edTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_project_task);

        etPTtask = (EditText) findViewById(R.id.etPTtask);
        etPTproject = (AutoCompleteTextView) findViewById(R.id.etPTproject);
        btnPTcancelCreate = (Button) findViewById(R.id.btnPTcancelCreate);
        btnPTcreate = (Button) findViewById(R.id.btnPTcreate);
        tvTitleNewProject = (TextView) findViewById(R.id.tvTitleNewProject);

        // Adapter that recommend projects that already exist
        String[] projects = new String[Variables.project.size()];
        int i = 0;
        for(String project : Variables.project) {
            projects[i] = project;
            i++;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, projects);
        etPTproject.setAdapter(adapter);

        // Bundle (if edit)
        final Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            tvTitleNewProject = (TextView) findViewById(R.id.tvTitleNewProject);
            tvTitleNewProject.setText(R.string.edit_task);
            btnPTcreate.setText(R.string.edit);
            edTask = bundle.getString("task");
            etPTtask.setText(edTask);
            edProject = bundle.getString("project");
            etPTproject.setText(edProject);
            edId = bundle.getInt("id");
            edTab = bundle.getInt("tab");
        }

        btnPTcancelCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnPTcreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etPTtask.getText().toString().isEmpty() || etPTproject.getText().toString().isEmpty()) {
                    Toast.makeText(InsertProjectTaskActivity.this, R.string.empty_fields, Toast.LENGTH_SHORT).show();
                } else {

                    if(bundle == null) {

                        // Create project task
                        Ion.with(getBaseContext())
                                .load(getString(R.string.uriProjectTasks))
                                .setHeader("Authorization", Variables.apiKey)
                                .setBodyParameter("task", etPTtask.getText().toString())
                                .setBodyParameter("project", etPTproject.getText().toString())
                                .setBodyParameter("state", "0")
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {

                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {
                                        GenericMessage2 res = new GenericMessage2();
                                        Gson gson = new Gson();
                                        res = gson.fromJson(result, res.getClass());

                                        if (res != null) {
                                            if (res.getError().equals("true")) {
                                                Toast.makeText(InsertProjectTaskActivity.this, R.string.error_create_task + res.getMessage(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                setResult(1);
                                                finish();
                                            }
                                        } else {
                                            Toast.makeText(InsertProjectTaskActivity.this, R.string.connection_error_add_task, Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                });

                    } else {

                        //Edit project task
                        Ion.with(getBaseContext())
                                .load("PUT", getString(R.string.uriProjectTasks) + "/" + edId)
                                .setHeader("Authorization", Variables.apiKey)
                                .setBodyParameter("task", etPTtask.getText().toString())
                                .setBodyParameter("project", etPTproject.getText().toString())
                                .setBodyParameter("state", String.valueOf(edTab))
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {

                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {

                                        GenericMessage2 res = new GenericMessage2();
                                        Gson gson = new Gson();
                                        res = gson.fromJson(result, res.getClass());

                                        if (res != null) {
                                            if (res.getError().equals("true")) {
                                                Toast.makeText(InsertProjectTaskActivity.this, R.string.error_update_task + res.getMessage(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                setResult(2);
                                                finish();
                                            }
                                        } else {
                                            Toast.makeText(InsertProjectTaskActivity.this, R.string.connection_error_update_task, Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                });

                    }
                }

            }
        });

    }
}
