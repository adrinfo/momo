package info.adri.momo;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import adapters.StatsAdapter;
import model.Stats;
import model.StatsMessage;
import model.Variables;


/**
 * <p class="description">Handles activity_stats.xml</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class StatsActivity extends AppCompatActivity {

    private RecyclerView rvStats;
    private TextView tvNoContentStats, tvTitleStats;
    private HashSet<String> tasksCompleted;

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);

        rvStats = (RecyclerView) findViewById(R.id.rvStats);
        tvNoContentStats = (TextView) findViewById(R.id.tvNoContentStats);
        tvTitleStats = (TextView) findViewById(R.id.tvTitleStats);
        tasksCompleted = new HashSet<>();

        Variables.statsAdapter = new StatsAdapter(this);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rvStats.setLayoutManager(llm);
        rvStats.addItemDecoration(new TodoTasksFragment.GapSpace(20));
        rvStats.setAdapter(Variables.statsAdapter);

        if(Variables.finalStats.isEmpty()) {
            thisWeek();
        }

        FloatingActionButton changeDate = (FloatingActionButton) findViewById(R.id.changeDate);
        changeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createStatsDateDialog().show();
            }
        });
    }

    public void thisWeek() {
        // Load this week stats
        Ion.with(this)
                .load("GET", getString(R.string.uriStats))
                .setHeader("Authorization", Variables.apiKey)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {

                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        StatsMessage res = new StatsMessage();
                        Gson gson = new Gson();
                        res = gson.fromJson(result, res.getClass());

                        if(res != null) {

                            if(res.getStats().isEmpty()) {

                                tvNoContentStats.setVisibility(View.VISIBLE);

                            } else {

                                tvNoContentStats.setVisibility(View.GONE);

                                for(Stats stats : res.getStats()) {
                                    tasksCompleted.add(stats.getTask());
                                    Variables.initialStats.add(stats);
                                    Log.d("STATS", stats.getTask() + " - " + stats.getGoalCompleted() + "/" + stats.getGoal());
                                }

                                for(String task : tasksCompleted) {
                                    int tempGoal = 0, tempGoalCompleted = 0;
                                    for(Stats stats : Variables.initialStats) {
                                        if(task.equals(stats.getTask())) {
                                            tempGoal = stats.getGoal();
                                            tempGoalCompleted += stats.getGoalCompleted();
                                        }
                                    }
                                    Date todayDate = new Date();
                                    Calendar todayCalendar = Calendar.getInstance();
                                    todayCalendar.setTime(todayDate);
                                    int today = todayCalendar.get(Calendar.DAY_OF_WEEK);

                                    // Sunday = 1
                                    if(today == 1)
                                        today = 8;

                                    int monday = Calendar.MONDAY;
                                    int difference = today - monday + 1;
                                    tempGoal = tempGoal * difference;

                                    Log.d("TAREA", "TASK: " + task + ", " + tempGoalCompleted + "/" + tempGoal);

                                    Variables.finalStats.add(new Stats(task, tempGoal, tempGoalCompleted));
                                }

                                tvTitleStats.setText(R.string.week_stats);
                                Variables.statsAdapter.notifyDataSetChanged();
                            }



                        } else {
                            Toast.makeText(getBaseContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                        }

                    }

                });
    }

    public void month() {

        // Load month stats
        Ion.with(this)
                .load("GET", getString(R.string.uriStats) + "/month")
                .setHeader("Authorization", Variables.apiKey)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {

                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        StatsMessage res = new StatsMessage();
                        Gson gson = new Gson();
                        res = gson.fromJson(result, res.getClass());

                        if(res != null) {

                            if(res.getStats().isEmpty()) {

                                tvNoContentStats.setVisibility(View.VISIBLE);

                            } else {

                                tvNoContentStats.setVisibility(View.GONE);

                                for(Stats stats : res.getStats()) {
                                    tasksCompleted.add(stats.getTask());
                                    Variables.initialStats.add(stats);
                                    Log.d("STATS", stats.getTask() + " - " + stats.getGoalCompleted() + "/" + stats.getGoal());
                                }

                                for(String task : tasksCompleted) {
                                    int tempGoal = 0, tempGoalCompleted = 0;
                                    for(Stats stats : Variables.initialStats) {
                                        if(task.equals(stats.getTask())) {
                                            tempGoal = stats.getGoal();
                                            tempGoalCompleted += stats.getGoalCompleted();
                                        }
                                    }
                                    Date todayDate = new Date();
                                    Calendar todayCalendar = Calendar.getInstance();
                                    todayCalendar.setTime(todayDate);
                                    int today = todayCalendar.get(Calendar.DAY_OF_MONTH);
                                    tempGoal = tempGoal * today;

                                    Log.d("TAREA", "TASK: " + task + ", " + tempGoalCompleted + "/" + tempGoal);

                                    Variables.finalStats.add(new Stats(task, tempGoal, tempGoalCompleted));
                                }

                                tvTitleStats.setText(R.string.month_stats);
                                Variables.statsAdapter.notifyDataSetChanged();
                            }



                        } else {
                            Toast.makeText(getBaseContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                        }

                    }

                });
    }

    public void year() {

        // Load month stats
        Ion.with(this)
                .load("GET", getString(R.string.uriStats) + "/year")
                .setHeader("Authorization", Variables.apiKey)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {

                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        StatsMessage res = new StatsMessage();
                        Gson gson = new Gson();
                        res = gson.fromJson(result, res.getClass());

                        if(res != null) {

                            if(res.getStats().isEmpty()) {

                                tvNoContentStats.setVisibility(View.VISIBLE);

                            } else {

                                tvNoContentStats.setVisibility(View.GONE);

                                for(Stats stats : res.getStats()) {
                                    tasksCompleted.add(stats.getTask());
                                    Variables.initialStats.add(stats);
                                    Log.d("STATS", stats.getTask() + " - " + stats.getGoalCompleted() + "/" + stats.getGoal());
                                }

                                for(String task : tasksCompleted) {
                                    int tempGoal = 0, tempGoalCompleted = 0;
                                    for(Stats stats : Variables.initialStats) {
                                        if(task.equals(stats.getTask())) {
                                            tempGoal = stats.getGoal();
                                            tempGoalCompleted += stats.getGoalCompleted();
                                        }
                                    }
                                    Date todayDate = new Date();
                                    Calendar todayCalendar = Calendar.getInstance();
                                    todayCalendar.setTime(todayDate);
                                    int today = todayCalendar.get(Calendar.DAY_OF_YEAR);
                                    tempGoal = tempGoal * today;

                                    Log.d("TAREA", "Today: "+ today + "TASK: " + task + ", " + tempGoalCompleted + "/" + tempGoal);

                                    Variables.finalStats.add(new Stats(task, tempGoal, tempGoalCompleted));
                                }

                                tvTitleStats.setText(R.string.year_stats);
                                Variables.statsAdapter.notifyDataSetChanged();
                            }



                        } else {
                            Toast.makeText(getBaseContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                        }

                    }

                });
    }

    public void reset() {
        tasksCompleted = new HashSet<>();
        Variables.initialStats = new ArrayList<>();
        Variables.finalStats = new ArrayList<>();
    }

    public AlertDialog createStatsDateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.date_range))
                .setItems(R.array.dates, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        switch(which) {
                            // This week
                            case 0:
                                reset();
                                thisWeek();
                                break;
                            // This month
                            case 1:
                                reset();
                                month();
                                break;
                            // This year
                            case 2:
                                reset();
                                year();
                                break;
                        }
                    }
                });

        return builder.create();

    }

}
