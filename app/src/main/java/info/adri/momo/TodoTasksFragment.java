package info.adri.momo;

import android.content.Intent;
import android.graphics.Rect;
import android.support.annotation.Nullable;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import adapters.TodoTaskAdapter;
import model.Variables;
import model.GenericMessage;
import model.TodoTask;
import model.TodoTasks;

/**
 * <p class="description">Handles fragment_todo_tasks.xml</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class TodoTasksFragment extends Fragment implements TodoTaskAdapter.ClickListener, TodoTaskAdapter.LongClickListener {

    static final int INSERTTODO = 1, UPDATETODO = 2;
    private int ilcPosition, ilcID;
    private ProgressBar pbTodo;
    private RecyclerView rvTodoTasks;
    private FloatingActionButton createTodo;
    private TextView tvNoContentTT;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Inflate and get main view
        View rootView;
        // If landscape
        if(getResources().getConfiguration().orientation == 2) {
            rootView = inflater.inflate(R.layout.fragment_todo_tasks_land, container, false);
        } else {
            rootView = inflater.inflate(R.layout.fragment_todo_tasks, container, false);
        }


        // Get views
        rvTodoTasks = (RecyclerView) rootView.findViewById(R.id.rvTodoTasks);
        createTodo = (FloatingActionButton) rootView.findViewById(R.id.createTodo);
        tvNoContentTT = (TextView) rootView.findViewById(R.id.tvNoContentTT);
        pbTodo = (ProgressBar) rootView.findViewById(R.id.pbTodo);

        // Floating button calls insert to do
        createTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), InsertTodoTaskActivity.class);
                startActivityForResult(i, INSERTTODO);
            }
        });

        // Instantiate adapter and add listener
        Variables.todoTaskAdapter = new TodoTaskAdapter(getContext());
        Variables.todoTaskAdapter.setClickListener(this);
        Variables.todoTaskAdapter.setLongClickListener(this);

        // Create layout manager and set to adapter
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rvTodoTasks.setLayoutManager(llm);
        rvTodoTasks.addItemDecoration(new GapSpace(20));
        rvTodoTasks.setAdapter(Variables.todoTaskAdapter);

        // Call data initialization
        dataInit();

        return rootView;
    }

    /**
     * Space between cards
     */
    public static class GapSpace extends RecyclerView.ItemDecoration {

        private final int verticalSpace;

        public GapSpace(int verticalSpace) {
            this.verticalSpace = verticalSpace;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.top = verticalSpace;
            outRect.left = verticalSpace;
            outRect.right = verticalSpace;
        }
    }

    /**
     * Fills the collections with data from the server
     */
    private void dataInit() {

        if(Variables.todoTasks.isEmpty()) {

            pbTodo.setVisibility(View.VISIBLE);
            // Load To Do tasks
            Ion.with(getContext())
                .load("GET", getString(R.string.uriTodoTasks))
                .setHeader("Authorization", Variables.apiKey)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {

                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        TodoTasks res = new TodoTasks();
                        Gson gson = new Gson();
                        res = gson.fromJson(result, res.getClass());

                        if(res != null) {

                            if(res.getTasks().isEmpty()) {

                                tvNoContentTT.setVisibility(View.VISIBLE);

                            } else {

                                tvNoContentTT.setVisibility(View.GONE);

                                for(TodoTask task : res.getTasks()) {
                                    Variables.todoCategories.add(task.getCategory());
                                    Variables.todoTasks.add(task);
                                    Log.d("TODOTASK", task.getTask() + ", category: " + task.getCategory() + ", completed: " + task.getCompleted());
                                }

                                Variables.todoTaskAdapter.notifyDataSetChanged();
                            }


                        } else {
                            Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                        }

                        pbTodo.setVisibility(View.GONE);

                    }

                });
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == INSERTTODO) {
            if (resultCode == 1) {
                Variables.todoTasks = new ArrayList<>();
                dataInit();
                Toast.makeText(getContext(), R.string.inserted_task, Toast.LENGTH_SHORT).show();
            }
        } else if(requestCode == UPDATETODO) {
            if(resultCode == 2) {
                Variables.todoTasks = new ArrayList<>();
                dataInit();
                Toast.makeText(getContext(), R.string.updated_task, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void itemClicked(View view, int position, int id) {

        final int pos = position;
        final View v = view;
        final int i = id;

        if(Variables.todoTasks.size() > 0) {
            if(view.isSelected()) {

                Log.d("ID-Selected:", "" + id + Variables.todoTasks.get(pos).getCompleted());

                // Incomplete to do task
                Ion.with(getContext())
                        .load("PUT", getString(R.string.uriTodoTasks) + "/" + id)
                        .setHeader("Authorization", Variables.apiKey)
                        .setBodyParameter("task", Variables.todoTasks.get(pos).getTask())
                        .setBodyParameter("completed", "0")
                        .setBodyParameter("category", Variables.todoTasks.get(pos).getCategory())
                        .setBodyParameter("priority", String.valueOf(Variables.todoTasks.get(pos).getPriority()))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {

                            @Override
                            public void onCompleted(Exception e, JsonObject result) {

                                GenericMessage res = new GenericMessage();
                                Gson gson = new Gson();
                                res = gson.fromJson(result, res.getClass());

                                if(res != null) {

                                    if(res.getError().equals("false")) {
                                        Variables.todoTasks = new ArrayList<>();
                                        dataInit();
                                    }

                                } else {
                                    v.setSelected(false);
                                    itemClicked(v, pos, i);
                                }

                            }

                        });

            } else {

                Log.d("ID-Unselected:", "" + id + Variables.todoTasks.get(pos).getCompleted());

                // Complete to do task
                Ion.with(getContext())
                        .load("PUT", getString(R.string.uriTodoTasks) + "/" + id)
                        .setHeader("Authorization", Variables.apiKey)
                        .setBodyParameter("task", Variables.todoTasks.get(pos).getTask())
                        .setBodyParameter("completed", "1")
                        .setBodyParameter("category", Variables.todoTasks.get(pos).getCategory())
                        .setBodyParameter("priority", String.valueOf(Variables.todoTasks.get(pos).getPriority()))
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {

                            @Override
                            public void onCompleted(Exception e, JsonObject result) {

                                GenericMessage res = new GenericMessage();
                                Gson gson = new Gson();
                                res = gson.fromJson(result, res.getClass());

                                if(res != null) {

                                    Log.d("Error", "" + res.getError());
                                    Log.d("Message", "" + res.getMessage());

                                    if(res.getError().equals("false")) {
                                        Variables.todoTasks = new ArrayList<>();
                                        dataInit();
                                    }

                                } else {
                                    Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                                }

                            }

                        });

            }
        }


    }

    @Override
    public void itemLongClicked(View view, int position, int id) {
        ilcPosition = position;
        ilcID = id;
        registerForContextMenu(view);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        if(Variables.todoTasks.get(ilcPosition).getCompleted() == 1) {
            inflater.inflate(R.menu.context_menu_remove, menu);
        } else {
            inflater.inflate(R.menu.context_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.ctxEdit:
                TodoTask tempTask = Variables.todoTasks.get(ilcPosition);
                Intent i = new Intent(getActivity(), InsertTodoTaskActivity.class);
                i.putExtra("task", tempTask.getTask());
                i.putExtra("category", tempTask.getCategory());
                i.putExtra("priority", tempTask.getPriority());
                i.putExtra("id", tempTask.getId());
                startActivityForResult(i, UPDATETODO);
                return true;
            case R.id.ctxRemove:
            case R.id.ctxRemoveRM:
                Log.d("ID", "" + ilcID);
                // Remove to do task
                Ion.with(getContext())
                        .load("DELETE", getString(R.string.uriTodoTasks) + "/" + ilcID)
                        .setHeader("Authorization", Variables.apiKey)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {

                            @Override
                            public void onCompleted(Exception e, JsonObject result) {

                                GenericMessage res = new GenericMessage();
                                Gson gson = new Gson();
                                res = gson.fromJson(result, res.getClass());

                                if(res != null) {

                                    Log.d("Error", "" + res.getError());
                                    Log.d("Message", "" + res.getMessage());

                                } else {
                                    Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                                }

                                Variables.todoTasks = new ArrayList<>();
                                dataInit();
                                Variables.todoTaskAdapter.notifyDataSetChanged();

                            }

                        });
                return true;
            case R.id.ctxRemoveAllRM:
                // Remove all to do tasks
                Ion.with(getContext())
                        .load("DELETE", getString(R.string.uriTodoTasks))
                        .setHeader("Authorization", Variables.apiKey)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {

                            @Override
                            public void onCompleted(Exception e, JsonObject result) {

                                GenericMessage res = new GenericMessage();
                                Gson gson = new Gson();
                                res = gson.fromJson(result, res.getClass());

                                if(res != null) {

                                    Log.d("Error", "" + res.getError());
                                    Log.d("Message", "" + res.getMessage());

                                } else {
                                    Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                                }

                                Variables.todoTasks = new ArrayList<>();
                                dataInit();
                                Variables.todoTaskAdapter.notifyDataSetChanged();

                            }

                        });

                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }
}
