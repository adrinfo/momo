package info.adri.momo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import model.DailyTask;
import model.DailyTaskCompleted;
import model.ProjectTask;
import model.Stats;
import model.TodoTask;
import model.Variables;

/**
 * <p class="description">Handles the fragments and the navigation elements when the user is logged in</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private FragmentManager fm;
    private FragmentTransaction ft;
    private TextView tvUserName, tvUserEmail;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        // Get the shared preferences
        prefs = getSharedPreferences("User", Context.MODE_PRIVATE);
        Variables.name = prefs.getString("name", "vacio");
        Variables.email = prefs.getString("email", "vacio");
        Variables.apiKey = prefs.getString("apiKey", "vacio");

        // Inflate the first fragment
        fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.content_frame, new DailyTasksFragment()).commit();

        if(savedInstanceState == null) {
            navigationView.getMenu().performIdentifierAction(R.id.nav_tareas_diarias, 0);
        }

        // Setup click events
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the ic_menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        // Set text to dynamic items
        tvUserName = (TextView) findViewById(R.id.user_name);
        tvUserEmail = (TextView) findViewById(R.id.user_email);

        tvUserName.setText(Variables.name);
        tvUserEmail.setText(Variables.email);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.log_out) {
            Variables.name = "";
            Variables.email = "";
            Variables.apiKey = "";
            Variables.chosenProject = "";
            Variables.project = new HashSet<>();
            Variables.todoCategories = new HashSet<>();
            Variables.dailyCategories = new HashSet<>();
            Variables.dailyTasks = new ArrayList<>();
            Variables.todoTasks = new ArrayList<>();
            Variables.initialStats = new ArrayList<>();
            Variables.finalStats = new ArrayList<>();
            Variables.dailyTaskCompleted = new ArrayList<>();
            Variables.projectTasksTodo = new ArrayList<>();
            Variables.projectTasksDoing = new ArrayList<>();
            Variables.projectTasksDone = new ArrayList<>();
            SharedPreferences preferences = getSharedPreferences("User", 0);
            preferences.edit().clear().commit();

            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
            return true;
        } else if (id == R.id.exit) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        FragmentManager fm = getSupportFragmentManager();

        int id = item.getItemId();

        if (id == R.id.nav_tareas_diarias) {
            fm.beginTransaction().replace(R.id.content_frame, new DailyTasksFragment()).commit();
        } else if (id == R.id.nav_tareas_a_hacer) {
            fm.beginTransaction().replace(R.id.content_frame, new TodoTasksFragment()).commit();
        } else if (id == R.id.nav_proyectos) {
            fm.beginTransaction().replace(R.id.content_frame, new ProjectTasksFragment()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
