package info.adri.momo;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import model.Variables;
import model.GenericMessage;
import model.GenericMessage2;

/**
 * <p class="description">Handles activity_insert_daily_task.xml</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class InsertDailyTaskActivity extends AppCompatActivity {

    private EditText etDTtask, etDTnumericValue;
    private AutoCompleteTextView etDTcategory;
    private Spinner spIcon;
    private ImageView ivIcon;
    private RadioButton rbDTboolean, rbDTnumeric;
    private Button btnDTcreate, btnDTcancelCreate;
    private String icon, edTask, edCategory, edIcon;
    private int type, edType, edGoal, edId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_daily_task);

        etDTtask = (EditText) findViewById(R.id.etDTtask);
        etDTcategory = (AutoCompleteTextView) findViewById(R.id.etDTcategory);
        spIcon = (Spinner) findViewById(R.id.spIcon);
        ivIcon = (ImageView) findViewById(R.id.ivIcon);
        rbDTboolean = (RadioButton) findViewById(R.id.rbDTboolean);
        rbDTnumeric= (RadioButton) findViewById(R.id.rbDTnumeric);
        etDTnumericValue = (EditText) findViewById(R.id.etDTnumericValue);
        btnDTcancelCreate = (Button) findViewById(R.id.btnDTcancelCreate);
        btnDTcreate = (Button) findViewById(R.id.btnDTcreate);

        // Adapter that recommend categories that already exist
        String[] categories = new String[Variables.dailyCategories.size()];
        int i = 0;
        for(String category : Variables.dailyCategories) {
            categories[i] = category;
            i++;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, categories);
        etDTcategory.setAdapter(adapter);

        // Default icon
        icon = "star";

        // Icons spinner
        ArrayAdapter<String> iconAdapter = new ArrayAdapter<String>(this, R.layout.daily_tasks_spinner, getResources().getStringArray(R.array.icons));
        spIcon.setAdapter(iconAdapter);
        int defaultPosition = iconAdapter.getPosition("Star");
        spIcon.setSelection(defaultPosition);

        // Bundle (if edit)
        final Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            TextView tvTitleNewDaily = (TextView) findViewById(R.id.tvTitleNewDaily);
            tvTitleNewDaily.setText(R.string.edit_task);
            btnDTcreate.setText(R.string.edit);
            edTask = bundle.getString("task");
            etDTtask.setText(edTask);
            edCategory = bundle.getString("category");
            etDTcategory.setText(edCategory);
            edIcon = bundle.getString("icon");
            String posIcon = edIcon.substring(0, 1).toUpperCase() + edIcon.substring(1);
            spIcon.setSelection(iconAdapter.getPosition(posIcon));
            edType = bundle.getInt("type");
            if(edType == 0) {
                rbDTboolean.setChecked(true);
            } else if(edType == 1){
                rbDTnumeric.setChecked(true);
                etDTnumericValue.setEnabled(true);
            }
            edGoal = bundle.getInt("goal");
            if(edType == 1) {
                etDTnumericValue.setText(String.valueOf(edGoal));
            }
            edId = bundle.getInt("id");
        }

        spIcon.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                icon = spIcon.getSelectedItem().toString().toLowerCase();
                ivIcon.setBackground(ContextCompat.getDrawable(getBaseContext(), getResources().getIdentifier(icon, "drawable", getPackageName())));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                ivIcon.setBackground(ContextCompat.getDrawable(getBaseContext(), R.drawable.star));
            }

        });


        rbDTboolean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 0;
                etDTnumericValue.setEnabled(false);
            }
        });
        rbDTnumeric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = 1;
                etDTnumericValue.setEnabled(true);
                etDTnumericValue.requestFocus();
            }
        });

        btnDTcancelCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        btnDTcreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etDTtask.getText().toString().isEmpty() || etDTcategory.getText().toString()
                        .isEmpty()) {

                    Toast.makeText(InsertDailyTaskActivity.this, R.string.empty_fields, Toast.LENGTH_SHORT)
                            .show();

                } else {

                    String goal;
                    if (type == 0) {
                        goal = "1";
                    } else {
                        goal = etDTnumericValue.getText().toString();
                    }

                    if (bundle == null) {

                        // Create daily task
                        Ion.with(getBaseContext())
                                .load(getString(R.string.uriDailyTasks))
                                .setHeader("Authorization", Variables.apiKey)
                                .setBodyParameter("task", etDTtask.getText().toString())
                                .setBodyParameter("category", etDTcategory.getText().toString())
                                .setBodyParameter("type", String.valueOf(type))
                                .setBodyParameter("goal", goal)
                                .setBodyParameter("icon", icon)
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {

                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {

                                        GenericMessage2 res = new GenericMessage2();
                                        Gson gson = new Gson();
                                        res = gson.fromJson(result, res.getClass());

                                        if (res != null) {
                                            if (res.getError().equals("true")) {
                                                Toast.makeText(InsertDailyTaskActivity.this, getString(R.string.error_create_task) + res.getMessage(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                setResult(1);
                                                finish();
                                            }
                                        } else {
                                            Toast.makeText(InsertDailyTaskActivity.this, R.string.connection_error_add_task, Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                });

                    } else {

                        // Update daily task
                        Ion.with(getBaseContext())
                                .load("PUT", getString(R.string.uriDailyTasks) + "/" + edId)
                                .setHeader("Authorization", Variables.apiKey)
                                .setBodyParameter("task", etDTtask.getText().toString())
                                .setBodyParameter("category", etDTcategory.getText().toString())
                                .setBodyParameter("type", String.valueOf(type))
                                .setBodyParameter("goal", goal)
                                .setBodyParameter("icon", icon)
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {

                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {

                                        GenericMessage res = new GenericMessage();
                                        Gson gson = new Gson();
                                        res = gson.fromJson(result, res.getClass());

                                        if (res != null) {
                                            if (res.getError().equals("true")) {
                                                Toast.makeText(InsertDailyTaskActivity.this, getString(R.string.error_update_task) + res.getMessage(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                setResult(2);
                                                finish();
                                            }
                                        } else {
                                            Toast.makeText(InsertDailyTaskActivity.this, R.string.connection_error_update_task, Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                });

                    }

                }

            };

        });
    }
}
