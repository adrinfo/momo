package info.adri.momo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * <p class="description">Controls the project tasks tabs</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class ProjectTasksFragment extends Fragment  {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int intItems = 3 ;
    public static View x;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Inflate project tasks and setup Views
        x = inflater.inflate(R.layout.fragment_project_tasks, null);
        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
        viewPager = (ViewPager) x.findViewById(R.id.viewpager);

        // Set an Adapter for the View Pager
        viewPager.setAdapter(new TabAdapter(getChildFragmentManager()));

        // Workaround: setupWithViewPager doesn't work without runnable
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });

        return x;

    }

    class TabAdapter extends FragmentPagerAdapter {

        public TabAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Return fragment relative to position
         */
        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            switch(position) {
                case 0: return new TabTodoFragment();
                case 1: return new TabDoingFragment();
                case 2: return new TabDoneFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return intItems;
        }

        /**
         * This method returns the title of the tab according to the position
         */
        @Override
        public CharSequence getPageTitle(int position) {
            switch(position) {
                case 0:
                    return getContext().getString(R.string.todo);
                case 1:
                    return getContext().getString(R.string.doing);
                case 2:
                    return getContext().getString(R.string.done);
            }
            return null;
        }
    }



}
