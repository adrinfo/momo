package info.adri.momo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.HashSet;

import adapters.ProjectTaskAdapterDone;
import model.Variables;
import model.GenericMessage;
import model.ProjectTask;
import model.ProjectTasks;

/**
 * <p class="description">Handles fragment_tab_done.xml</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class TabDoneFragment extends Fragment implements ProjectTaskAdapterDone.LongClickListener {

    private RecyclerView rvProjectTasksDone;
    private int ilcPosition, ilcID;
    static final int UPDATEPROJECT = 2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate and get main view

        View rootView;

        // If landscape
        if(getResources().getConfiguration().orientation == 2) {
            rootView = inflater.inflate(R.layout.fragment_tab_done_land, container, false);
        } else {
            rootView = inflater.inflate(R.layout.fragment_tab_done, container, false);
        }

        // Get views
        rvProjectTasksDone = (RecyclerView) rootView.findViewById(R.id.rvProjectTasksDone);

        // Instantiate adapter and add listener
        Variables.projectTaskAdapterDone = new ProjectTaskAdapterDone(getContext());
        Variables.projectTaskAdapterDone.setLongClickListener(this);

        // Create layout manager and set to adapter
        LinearLayoutManager llmd = new LinearLayoutManager(getActivity());
        rvProjectTasksDone.setLayoutManager(llmd);
        rvProjectTasksDone.addItemDecoration(new TodoTasksFragment.GapSpace(20));
        rvProjectTasksDone.setAdapter(Variables.projectTaskAdapterDone);

        return rootView;
    }

    /**
     * Fills the collections with data from the server
     */
    private void dataInit() {

        if(Variables.projectTasksTodo.isEmpty() && Variables.projectTasksDoing.isEmpty() && Variables.projectTasksDone.isEmpty()) {
            // Load project tasks
            Ion.with(getContext())
                    .load("GET", getString(R.string.uriProjectTasks))
                    .setHeader("Authorization", Variables.apiKey)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {

                        @Override
                        public void onCompleted(Exception e, JsonObject result) {

                            ProjectTasks res = new ProjectTasks();
                            Gson gson = new Gson();
                            res = gson.fromJson(result, res.getClass());

                            if(res != null) {
                                for(ProjectTask task : res.getTasks()) {
                                    Variables.project.add(task.getProject());
                                }
                                if(Variables.chosenProject.isEmpty()) {
                                    Variables.chosenProject = Variables.project.toArray()[0].toString();
                                }
                                Log.d("chosenProject", Variables.chosenProject);
                                for(ProjectTask task: res.getTasks()) {
                                    if(task.getProject().equals(Variables.chosenProject)) {
                                        switch(task.getState()) {
                                            case 0:
                                                Variables.projectTasksTodo.add(task);
                                                break;
                                            case 1:
                                                Variables.projectTasksDoing.add(task);
                                                break;
                                            case 2:
                                                Variables.projectTasksDone.add(task);
                                                break;
                                        }
                                        Log.d("PROJECTTASK", task.getTask() + ", project: " + task.getProject() + ", state: " + task.getState());
                                    }
                                }
                                Log.d("HASHSET", Variables.project.toString());

                                Variables.projectTaskAdapterTodo.notifyDataSetChanged();
                                Variables.projectTaskAdapterDoing.notifyDataSetChanged();
                                Variables.projectTaskAdapterDone.notifyDataSetChanged();

                            } else {
                                Toast.makeText(getContext(), "Error en la conexión", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
        }

    }

    /**
     * Clears the collections
     */
    public void reset() {

        Variables.project = new HashSet<>();
        Variables.projectTasksTodo = new ArrayList<>();
        Variables.projectTasksDoing = new ArrayList<>();
        Variables.projectTasksDone = new ArrayList<>();

    }

    @Override
    public void itemLongClicked(View view, int position, int id) {
        ilcPosition = position;
        ilcID = id;
        registerForContextMenu(view);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == UPDATEPROJECT) {
            if(resultCode == 2) {
                reset();
                dataInit();
                Toast.makeText(getContext(), R.string.updated_task, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if (getUserVisibleHint()) {

            switch (item.getItemId()) {

                case R.id.ctxEdit:
                    ProjectTask tempTask = Variables.projectTasksDone.get(ilcPosition);
                    Intent i = new Intent(getActivity(), InsertProjectTaskActivity.class);
                    i.putExtra("task", tempTask.getTask());
                    i.putExtra("project", tempTask.getProject());
                    i.putExtra("id", tempTask.getId());
                    i.putExtra("tab", 2);
                    startActivityForResult(i, UPDATEPROJECT);
                    return true;

                case R.id.ctxRemove:

                    Log.d("ID", "" + ilcID);

                    // Remove to do task
                    Ion.with(getContext())
                            .load("DELETE", getString(R.string.uriProjectTasks) + "/" + ilcID)
                            .setHeader("Authorization", Variables.apiKey)
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {

                                @Override
                                public void onCompleted(Exception e, JsonObject result) {

                                    GenericMessage res = new GenericMessage();
                                    Gson gson = new Gson();
                                    res = gson.fromJson(result, res.getClass());

                                    if (res != null) {

                                        Log.d("Error", "" + res.getError());
                                        Log.d("Message", "" + res.getMessage());

                                    } else {
                                        Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                                    }

                                    reset();
                                    dataInit();

                                }

                            });

                    return true;

                default:
                    return super.onContextItemSelected(item);

            }

        }

        return super.onContextItemSelected(item);
    }

}
