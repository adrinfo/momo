package info.adri.momo;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import adapters.DailyTaskAdapter;
import model.Variables;
import model.DailyTask;
import model.DailyTaskCompleted;
import model.DailyTasks;
import model.DailyTasksCompleted;
import model.GenericMessage;
import model.GenericMessage2;

/**
 * <p class="description">Handles fragment_daily_tasks.xml</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class DailyTasksFragment extends Fragment implements DailyTaskAdapter.ClickListener, DailyTaskAdapter.LongClickListener {

    private static final int INSERTDAILY = 1, UPDATEDAILY = 2;
    private int loaded, ilcPosition, ilcID, tempGoal;
    private boolean invisible;
    private ProgressBar pbDaily;
    private AlertDialog dialog;
    private TextView tvNoContentDT;
    private RecyclerView rvDailyTasks;
    private FloatingActionButton createDaily, showStats, switchButtons;
    private FrameLayout flSwitch;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Inflate and get main view
        View rootView = inflater.inflate(R.layout.fragment_daily_tasks, container, false);

        // If loaded 0, there isn't collection uploaded yet
        loaded = 0;
        // If invisible true only one floating button is showed
        invisible = true;

        // Get views
        rvDailyTasks = (RecyclerView) rootView.findViewById(R.id.rvDailyTasks);
        tvNoContentDT = (TextView) rootView.findViewById(R.id.tvNoContentDT);
        pbDaily = (ProgressBar) rootView.findViewById(R.id.pbDaily);
        createDaily = (FloatingActionButton) rootView.findViewById(R.id.createDaily);
        showStats = (FloatingActionButton) rootView.findViewById(R.id.showStats);
        switchButtons = (FloatingActionButton) rootView.findViewById(R.id.switchButtons);
        flSwitch = (FrameLayout) rootView.findViewById(R.id.flSwitch);

        // Instantiate adapter and add listener
        Variables.dailyTaskAdapter = new DailyTaskAdapter(getContext(), getActivity());
        Variables.dailyTaskAdapter.setClickListener(this);
        Variables.dailyTaskAdapter.setLongClickListener(this);

        // Create layout manager and set to adapter
        // If landscape
        GridLayoutManager glm;
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        if(getResources().getConfiguration().orientation == 2) {
            if(display.getWidth() < 1000) {
                glm = new GridLayoutManager(getContext(), 4);
            } else {
                glm = new GridLayoutManager(getContext(), 5);
            }
        } else {
            if(display.getWidth() < 500) {
                glm = new GridLayoutManager(getContext(), 2);
            } else {
                Log.d("HEEEEEEY", "" + display.getWidth());
                glm = new GridLayoutManager(getContext(), 3);
            }
        }
        rvDailyTasks.setLayoutManager(glm);
        rvDailyTasks.setAdapter(Variables.dailyTaskAdapter);

        // Call data initialization
        dataInit();

        // Shows and hides the other buttons
        switchButtons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(invisible) {
                    flSwitch.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        v.setElevation(6);
                        v.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryLight)));
                    }
                    invisible = false;
                } else {
                    flSwitch.setVisibility(View.GONE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        v.setElevation(12);
                        v.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                    }
                    invisible = true;
                }
            }


        });

        flSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flSwitch.setVisibility(View.GONE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    switchButtons.setElevation(12);
                    switchButtons.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                }
                invisible = true;
            }
        });

        // Starts activity to insert a daily task
        createDaily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flSwitch.setVisibility(View.GONE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    switchButtons.setElevation(12);
                    switchButtons.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                }
                invisible = true;

                Intent i = new Intent(getContext(), InsertDailyTaskActivity.class);
                startActivityForResult(i, INSERTDAILY);
            }
        });

        // Starts activity to view statistics
        showStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flSwitch.setVisibility(View.GONE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    switchButtons.setElevation(12);
                    switchButtons.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                }
                invisible = true;

                Intent i = new Intent(getContext(), StatsActivity.class);
                startActivity(i);
            }
        });

        return rootView;

    }

    /**
     * Fills the collections with data from the server
     */
    private void dataInit() {

        if(Variables.dailyTasks.isEmpty()) {

            pbDaily.setVisibility(View.VISIBLE);

            // Load daily tasks
            Ion.with(getContext())
                .load("GET", getString(R.string.uriDailyTasks))
                .setHeader("Authorization", Variables.apiKey)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {

                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        DailyTasks res = new DailyTasks();
                        Gson gson = new Gson();
                        res = gson.fromJson(result, res.getClass());

                        if(res != null) {

                            for(DailyTask task : res.getTasks()) {
                                Variables.dailyCategories.add(task.getCategory());
                                Variables.dailyTasks.add(task);
                            }

                            loaded++;

                            if(loaded == 2) {
                                mixEm();
                            }

                        }

                    }

                });

            // Load completed daily tasks
            Ion.with(getContext())
                .load("GET", getString(R.string.uriDailyTasksCompleted))
                .setHeader("Authorization", Variables.apiKey)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {

                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        DailyTasksCompleted res = new DailyTasksCompleted();
                        Gson gson = new Gson();
                        res = gson.fromJson(result, res.getClass());

                        if(res != null) {

                            for(DailyTaskCompleted completed : res.getTasks()) {

                                Variables.dailyTaskCompleted.add(completed);

                            }

                            loaded++;

                            if(loaded == 2) {
                                mixEm();
                            }
                        }

                    }

                });

        }

    }

    /**
     * Have to mix two collections: dailyTasks and dailyTasksCompleted
     */
    public void mixEm() {

        if(Variables.dailyTasks.isEmpty()) {

            tvNoContentDT.setVisibility(View.VISIBLE);

        } else {

            tvNoContentDT.setVisibility(View.GONE);

            for(DailyTask task : Variables.dailyTasks) {

                for(DailyTaskCompleted completed : Variables.dailyTaskCompleted) {

                    if(task.getId() == completed.getDailyTaskId()) {
                        task.setCompleted(1);
                    }

                }

            }

            Variables.dailyTaskAdapter.notifyDataSetChanged();
        }

        pbDaily.setVisibility(View.GONE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == INSERTDAILY) {
            if(resultCode == 1) {
                Variables.dailyTasks = new ArrayList<>();
                loaded = 0;
                dataInit();
                Toast.makeText(getContext(), R.string.inserted_task, Toast.LENGTH_SHORT).show();
            }
        } else if(requestCode == UPDATEDAILY) {
            if(resultCode == 2) {
                Variables.dailyTasks = new ArrayList<>();
                loaded = 0;
                dataInit();
                Toast.makeText(getContext(), R.string.updated_task, Toast.LENGTH_SHORT).show();
            }
        }

        // Reinitiate stats
        Variables.finalStats = new ArrayList<>();
        Variables.initialStats = new ArrayList<>();

    }

    @Override
    public void itemClicked(View view, int position, int id, int goal) {

        ArrayList<String> dailys = new ArrayList<>();
        ArrayList<String> completed = new ArrayList<>();

        for(DailyTask task : Variables.dailyTasks) {
            dailys.add(task.getId() + "- " + task.getTask());
        }

        for(DailyTaskCompleted dCompleted : Variables.dailyTaskCompleted) {
            completed.add(dCompleted.getId() + "- " + dCompleted.getDailyTaskId());
        }

        if(view.isSelected()) {

            // Delete completed daily task
            Ion.with(getContext())
                    .load("DELETE", getString(R.string.uriDailyTasksCompleted) + "/" + id)
                    .setHeader("Authorization", Variables.apiKey)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {

                        @Override
                        public void onCompleted(Exception e, JsonObject result) {

                            GenericMessage res = new GenericMessage();
                            Gson gson = new Gson();
                            res = gson.fromJson(result, res.getClass());

                            if(res != null) {

                                Log.d("Error", "" + res.getError());
                                Log.d("Message", "" + res.getMessage());

                                if(res.getError().equals("false")) {
                                    Variables.dailyTasks = new ArrayList<>();
                                    Variables.dailyTaskCompleted = new ArrayList<>();
                                    loaded = 0;
                                    dataInit();
                                }

                            } else {
                                Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                            }

                        }

                    });

        } else {

            int taskGoal = Variables.dailyTasks.get(position).getGoal();
            if(taskGoal != 1) {
                createGoalDialog(id);
            } else {

                // Insert completed daily task
                Ion.with(getContext())
                        .load(getString(R.string.uriDailyTasksCompleted))
                        .setHeader("Authorization", Variables.apiKey)
                        .setBodyParameter("daily_task_id", String.valueOf(id))
                        .setBodyParameter("goal", "1")
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {

                            @Override
                            public void onCompleted(Exception e, JsonObject result) {

                                GenericMessage2 res = new GenericMessage2();
                                Gson gson = new Gson();
                                res = gson.fromJson(result, res.getClass());

                                if(res != null) {

                                    Log.d("Error", "" + res.getError());
                                    Log.d("Message", "" + res.getMessage());
                                    Log.d("TaskId", "" + res.getTaskId());

                                    if(res.getError().equals("false")) {
                                        Variables.dailyTasks = new ArrayList<>();
                                        Variables.dailyTaskCompleted = new ArrayList<>();
                                        loaded = 0;
                                        dataInit();
                                    }

                                } else {
                                    Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                                }

                            }

                        });
            }
        }

    }



    @Override
    public void itemLongClicked(View view, int position, int id) {
        ilcPosition = position;
        ilcID = id;
        registerForContextMenu(view);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.ctxEdit:
                DailyTask tempTask = Variables.dailyTasks.get(ilcPosition);
                Intent i = new Intent(getActivity(), InsertDailyTaskActivity.class);
                i.putExtra("task", tempTask.getTask());
                i.putExtra("category", tempTask.getCategory());
                i.putExtra("type", tempTask.getType());
                i.putExtra("goal", tempTask.getGoal());
                i.putExtra("icon", tempTask.getIcon());
                i.putExtra("id", tempTask.getId());
                startActivityForResult(i, UPDATEDAILY);
                return true;
            case R.id.ctxRemove:
                // Remove daily task
                Ion.with(getContext())
                    .load("DELETE", getString(R.string.uriDailyTasks) + "/" + ilcID)
                    .setHeader("Authorization", Variables.apiKey)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {

                        @Override
                        public void onCompleted(Exception e, JsonObject result) {

                            GenericMessage res = new GenericMessage();
                            Gson gson = new Gson();
                            res = gson.fromJson(result, res.getClass());

                            if(res != null) {

                                Log.d("Error", "" + res.getError());
                                Log.d("Message", "" + res.getMessage());

                            } else {
                                Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                            }

                            Variables.dailyTasks = new ArrayList<>();
                            loaded = 0;
                            dataInit();
                            Variables.dailyTaskAdapter.notifyDataSetChanged();

                        }

                    });
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /**
     * Creates dialog for completing numeric daily tasks
     */
    private void createGoalDialog(final int id) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_ask_goal, null);
        builder.setView(v);

        final EditText etGoal = (EditText) v.findViewById(R.id.etGoal);
        Button btnGoalOk = (Button) v.findViewById(R.id.btnGoalOk);
        Button btnGoalCancel = (Button) v.findViewById(R.id.btnGoalCancel);

        dialog = builder.create();

        btnGoalOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(etGoal.getText().toString().isEmpty()) {
                    Toast.makeText(getContext(), R.string.introduce_number, Toast.LENGTH_SHORT).show();
                } else {

                    tempGoal = Integer.parseInt(etGoal.getText().toString());

                    if(tempGoal != 1) {

                        // Insert completed daily task
                        Ion.with(getContext())
                            .load(getString(R.string.uriDailyTasksCompleted))
                            .setHeader("Authorization", Variables.apiKey)
                            .setBodyParameter("daily_task_id", String.valueOf(id))
                            .setBodyParameter("goal", String.valueOf(tempGoal))
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {

                                @Override
                                public void onCompleted(Exception e, JsonObject result) {

                                    GenericMessage2 res = new GenericMessage2();
                                    Gson gson = new Gson();
                                    res = gson.fromJson(result, res.getClass());

                                    if (res != null) {

                                        Log.d("Error", "" + res.getError());
                                        Log.d("Message", "" + res.getMessage());
                                        Log.d("TaskId", "" + res.getTaskId());

                                        Variables.dailyTasks = new ArrayList<>();
                                        loaded = 0;
                                        dataInit();

                                    } else {
                                        Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                                    }

                                    dialog.dismiss();

                                }

                            });

                    }

                }
            }
        });

        btnGoalCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

}
