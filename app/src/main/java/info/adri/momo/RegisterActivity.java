package info.adri.momo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import model.GenericMessage;

/**
 * <p class="description">Handles activity_register_activity.xml</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class RegisterActivity extends AppCompatActivity {

    EditText etNameR, etEmailR, etPassR;
    Button btnRegistrarse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_register_activity);

        etNameR = (EditText) findViewById(R.id.etNameR);
        etEmailR = (EditText) findViewById(R.id.etEmailR);
        etPassR = (EditText) findViewById(R.id.etPassR);
        btnRegistrarse = (Button) findViewById(R.id.btnRegistrarse);

        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(etNameR.getText().toString().isEmpty() || etEmailR.getText().toString().isEmpty() || etPassR.getText().toString().isEmpty()) {
                    Toast.makeText(RegisterActivity.this, R.string.empty_fields, Toast.LENGTH_SHORT).show();
                } else {
                    if(etEmailR.getText().toString().contains("@") && etEmailR.getText().toString().contains(".")) {
                        Ion.with(getBaseContext())
                                .load(getString(R.string.uriRegister))
                                .setBodyParameter("name", etNameR.getText().toString())
                                .setBodyParameter("email", etEmailR.getText().toString())
                                .setBodyParameter("password", etPassR.getText().toString())
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {

                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {

                                        GenericMessage res = new GenericMessage();
                                        Gson gson = new Gson();
                                        res = gson.fromJson(result, res.getClass());

                                        if(res != null) {
                                            if(res.getError().equals("true")) {
                                                Toast.makeText(RegisterActivity.this, "Datos incorrectos", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(RegisterActivity.this, "Registrado correctamente", Toast.LENGTH_SHORT).show();
                                                finish();
                                            }
                                        } else {
                                            Toast.makeText(RegisterActivity.this, "Error en la conexión. No se ha podido realizar registro.", Toast.LENGTH_SHORT).show();
                                        }


                                    }

                                });

                        finish();
                    } else {
                        Toast.makeText(RegisterActivity.this, R.string.error_email, Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
    }
}
