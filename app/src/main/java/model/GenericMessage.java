package model;

/**
 * <p class="description">Structure of a generic message response</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class GenericMessage {

    private String error, message;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
