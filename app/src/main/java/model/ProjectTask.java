package model;

/**
 * <p class="description">Structure of a project task</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class ProjectTask {

    private int id, state;
    private String task, project;

    public ProjectTask() {
    }

    public ProjectTask(int id, int state, String task, String project) {
        this.id = id;
        this.state = state;
        this.task = task;
        this.project = project;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }
}
