package model;

import java.util.Date;

/**
 * <p class="description">Structure of a daily task</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class DailyTask {

    String task, category, icon, error;
    int id, type, goal, completed;

    public DailyTask() {
    }

    public DailyTask(String task, String category, String icon, int type, int goal, int id) {
        this.task = task;
        this.category = category;
        this.icon = icon;
        this.type = type;
        this.goal = goal;
        this.completed = 0;
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
