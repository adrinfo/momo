package model;

/**
 * <p class="description">Structure of stats</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class Stats {

    private int goal, completedgoal;
    private String task;

    public Stats() {
    }

    public Stats(String task, int goal, int completedGoal) {
        this.task = task;
        this.goal = goal;
        this.completedgoal = completedGoal;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }

    public int getGoalCompleted() {
        return completedgoal;
    }

    public void setGoalCompleted(int completedgoal) {
        this.completedgoal = completedgoal;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }
}
