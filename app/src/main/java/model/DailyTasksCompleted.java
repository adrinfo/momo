package model;

import java.util.ArrayList;

/**
 * <p class="description">Structure of a completed daily tasks</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class DailyTasksCompleted {

    private String error;
    private ArrayList<DailyTaskCompleted> tasks;

    public ArrayList<DailyTaskCompleted> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<DailyTaskCompleted> tasks) {
        this.tasks = tasks;
    }
}
