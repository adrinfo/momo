package model;

import java.util.ArrayList;

/**
 * <p class="description">Structure of project tasks response</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class ProjectTasks {

    private ArrayList<ProjectTask> tasks;
    private String error;

    public ArrayList<ProjectTask> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<ProjectTask> tasks) {
        this.tasks = tasks;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
