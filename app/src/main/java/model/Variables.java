package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import adapters.DailyTaskAdapter;
import adapters.ProjectTaskAdapterDoing;
import adapters.ProjectTaskAdapterDone;
import adapters.ProjectTaskAdapterTodo;
import adapters.StatsAdapter;
import adapters.TodoTaskAdapter;

/**
 * <p class="description">Variables available for all activities</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class Variables {

    public static String name = "";
    public static String email = "";
    public static String apiKey = "";
    public static String chosenProject = "";
    public static HashSet<String> project = new HashSet<>();
    public static HashSet<String> todoCategories = new HashSet<>();
    public static HashSet<String> dailyCategories = new HashSet<>();
    public static List<DailyTask> dailyTasks = new ArrayList<>();
    public static List<TodoTask> todoTasks = new ArrayList<>();
    public static List<Stats> initialStats = new ArrayList<>();
    public static List<Stats> finalStats = new ArrayList<>();
    public static List<DailyTaskCompleted> dailyTaskCompleted = new ArrayList<>();
    public static List<ProjectTask> projectTasksTodo = new ArrayList<>();
    public static List<ProjectTask> projectTasksDoing = new ArrayList<>();
    public static List<ProjectTask> projectTasksDone = new ArrayList<>();
    public static DailyTaskAdapter dailyTaskAdapter;
    public static TodoTaskAdapter todoTaskAdapter;
    public static StatsAdapter statsAdapter;
    public static ProjectTaskAdapterTodo projectTaskAdapterTodo;
    public static ProjectTaskAdapterDoing projectTaskAdapterDoing;
    public static ProjectTaskAdapterDone projectTaskAdapterDone;


}
