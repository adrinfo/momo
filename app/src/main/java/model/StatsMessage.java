package model;

import java.util.ArrayList;

/**
 * <p class="description">Structure of stats message</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class StatsMessage {

    String error;
    ArrayList<Stats> stats;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ArrayList<Stats> getStats() {
        return stats;
    }

    public void setStats(ArrayList<Stats> stats) {
        this.stats = stats;
    }
}
