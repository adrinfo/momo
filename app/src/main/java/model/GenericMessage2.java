package model;

/**
 * <p class="description">Structure of a generic message (2) response</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class GenericMessage2 {

    private String error, message, taskId;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
