package model;

/**
 * <p class="description">Structure of a To Do Task</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class TodoTask {

    private int id, completed, priority;
    private String task, category, error;

    public TodoTask() {
    }

    public TodoTask(int id, int completed, int priority, String task, String category) {
        this.id = id;
        this.completed = completed;
        this.priority = priority;
        this.task = task;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
