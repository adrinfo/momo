package model;

import java.util.ArrayList;

/**
 * <p class="description">All daily tasks</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class DailyTasks {

    String error;
    ArrayList<DailyTask> tasks;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ArrayList<DailyTask> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<DailyTask> tasks) {
        this.tasks = tasks;
    }
}
