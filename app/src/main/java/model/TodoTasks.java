package model;

import java.util.ArrayList;

/**
 * <p class="description">All To Do Tasks</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class TodoTasks {

    private String error;
    private ArrayList<TodoTask> tasks;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ArrayList<TodoTask> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<TodoTask> tasks) {
        this.tasks = tasks;
    }

}
