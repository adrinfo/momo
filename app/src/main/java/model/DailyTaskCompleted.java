package model;

import java.sql.Timestamp;
import java.util.Date;

/**
 * <p class="description">Structure of a completed daily task</p>
 * @author Adriano L.G.
 * @version 1.0
 */
public class DailyTaskCompleted {

    private int id, dailyTaskId, goal;
    private String createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDailyTaskId() {
        return dailyTaskId;
    }

    public void setDailyTaskId(int dailyTaskId) {
        this.dailyTaskId = dailyTaskId;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
